class Grid {
    constructor (options) {
        this.numOfRows = options.numOfRows;
        this.numOfColumns = options.numOfColumns;
        this.targetElement = options.targetElement || document.body;
        this.cellClasses = options.cellClasses || [];
        this.gridElement = this.createGridElement();
        this.rows = [];
        this.createRows();
        this.gridElement.addEventListener("click", this.clickCell.bind(this));
    }

    clickCell(event) {
        let clickedCell = event.target;
        if(!clickedCell.classList.contains("cell")) return;
        clickedCell.classList.add("clickedCell");

        let rowIndex = Number(clickedCell.dataset.rowIndex);
        let colIndex = Number(clickedCell.dataset.colIndex);
        clickedCell = this.findCellIndex(rowIndex, colIndex);
        console.log("Cell Index: ", clickedCell);

        const north = this.findCellIndex(rowIndex - 1, colIndex);
        const northEast = this.findCellIndex(rowIndex - 1, colIndex + 1);
        const east = this.findCellIndex(rowIndex, colIndex + 1);
        const southEast = this.findCellIndex(rowIndex + 1, colIndex + 1);
        const south = this.findCellIndex(rowIndex + 1, colIndex);
        const southWest = this.findCellIndex(rowIndex + 1, colIndex -1);
        const west = this.findCellIndex(rowIndex, colIndex - 1);
        const northWest = this.findCellIndex(rowIndex - 1, colIndex - 1);
        const clickedCellNeighbors = [north, northEast, east, southEast, south, southWest, west, northWest];
        console.log("Neighbor Indexes (NEVER EAT SOGGY WAFFLES): ", clickedCellNeighbors);
    }
    createGridElement() {
        const element = document.createElement("div");
        element.classList.add("grid");
        this.targetElement.appendChild(element);
        return element;
    }
    createRowElement(rowIndex) {
        const element = document.createElement("div");
        element.classList.add("row");
        element.dataset.rowIndex = rowIndex;
        this.gridElement.appendChild(element);
        return element;
    }
    createRows() {
        for (let rowIndex = 0; rowIndex < this.numOfRows; rowIndex++) {
            this.rows[rowIndex] = [];
            const rowElement = this.createRowElement(rowIndex);
            this.createCells(rowIndex, rowElement);
        }
    }
    createCells(rowIndex, rowElement) {
        for (let colIndex = 0; colIndex < this.numOfColumns; colIndex++) {
            const cell = new Cell(rowIndex, colIndex, this.cellClasses);
            this.rows[rowIndex][colIndex] = cell;
            rowElement.appendChild(cell.element);
        }
    }
    findCellIndex(rowIndex, colIndex) {
        rowIndex = Number(rowIndex);
        colIndex = Number(colIndex);
        if(this.rows[rowIndex]) {
            if(this.rows[rowIndex][colIndex]) {
                return this.rows[rowIndex][colIndex];
            }
        }
        return null; 
    }
}